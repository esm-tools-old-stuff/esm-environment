#!/usr/bin/env bash

set -e

export useMPI=$1
export ENVIRONMENT_SET_BY_ESMTOOLS=TRUE

DIR=`pwd`

# if an arg is given, use it as hostname
if [ -z "$2" ]; then
   # no argument given
   LOGINHOST="$(hostname -f)"
else
   LOGINHOST=$2
fi

if [[ $LOGINHOST =~ ^m[A-Za-z0-9]+\.hpc\.dkrz\.de$ ]]; then
   STRATEGY="mistral.dkrz.de"
elif [[ $LOGINHOST =~ ^ollie[0-9]$ ]] || [[ $LOGINHOST =~ ^prod-[0-9]{4}$ ]]; then
   STRATEGY="ollie.awi.de"
elif [[ $LOGINHOST =~ ^g[A-Za-z0-9]+\.usr\.hlrn\.de$ ]]; then
   STRATEGY="glogin.hlrn.de"
elif [[ $LOGINHOST =~ ^b[A-Za-z0-9]+\.hsn\.hlrn\.de$ ]]; then
   STRATEGY="blogin.hlrn.de"
elif [[ $LOGINHOST =~ \.hww\.de$ ]] || [[ $LOGINHOST =~ ^nid[0-9]{5}$ ]]; then
   STRATEGY="hazelhen.hww.de"
elif [[  $LOGINHOST =~ \.jureca$ ]]; then
   STRATEGY="jureca.fz-juelich.de"
elif [[ $LOGINHOST =~ ^juwels[0-9][0-9]\.fz-juelich\.de$ ]] || [[ $LOGINHOST =~ ^juwels[0-9][0-9]\.ib.juwels.fzj.de ]] ; then
   STRATEGY="juwels.fz-juelich.de"
else
   echo "can not determine environment for host: "$LOGINHOST
   [ $BEING_EXECUTED == true ] && exit 1
   return # if we are being sourced, return from this script here
fi

source $DIR/$STRATEGY $useMPI
